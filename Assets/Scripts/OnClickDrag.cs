﻿using UnityEngine;

public class OnClickDrag : MonoBehaviour
{

    public Vector3 worldPos, startPos, mousePos;
    Transform moveTransform;
    public float rotationalSnapThreshold = 1f;

    private void Start()
    {
        moveTransform = transform.parent != null ? transform.parent : transform;
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButton(2))
        {
            startPos = Camera.main.WorldToScreenPoint(transform.position);
        }
    }

    private void OnMouseDrag()
    {
        mousePos = Input.mousePosition;

        if (Input.GetMouseButton(0))
        {
            mousePos.z = -Camera.main.transform.position.z;
            worldPos = Camera.main.ScreenToWorldPoint(mousePos);
            moveTransform.position = worldPos;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(1))
        {
            if (Vector2.Distance(startPos, mousePos) > rotationalSnapThreshold)
            {
                Vector2 direction = mousePos - startPos;

                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                {
                    if (direction.x > 0)
                    {
                        ModifyLocalRotation(0, 90);
                    }
                    else
                    {
                        ModifyLocalRotation(0, -90);
                    }
                }
                else
                {
                    if (direction.y > 0)
                    {
                        ModifyLocalRotation(90, 0);
                    }
                    else
                    {
                        ModifyLocalRotation(-90, 0);
                    }
                }
                startPos = Input.mousePosition;
            }
        }  
    }
    void ModifyLocalRotation(int x, int z)
    {
        transform.localRotation = Quaternion.Euler(transform.localRotation.x + x, transform.localRotation.y, transform.localRotation.z + z);
    }
}
