﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMover : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private Transform BoxDestination;

    [SerializeField]
    private int BoxMoveSpeed;

    public int LocationIndex = 0;
    void Awake()
    {
        ConveyerBeltSystems.Instance.AssignBoxNextDesination(this.gameObject);
    }

    public void StartMoving()
    {
        StartCoroutine(moveBox());
    }

    private IEnumerator moveBox()
    {
        while (this.transform.position != BoxDestination.position)
        {
            float step = Time.deltaTime * BoxMoveSpeed;

            this.transform.position =
                Vector3.MoveTowards(this.transform.position,
                BoxDestination.position, step);

            yield return null;
        }
        //yield return new WaitForEndOfFrame();

        //GetComponent<Rigidbody>().AddForce(Vector3.right * 20f, ForceMode.Impulse);

        //Once box reached its destination, 
        //it should request the belt system for its next destination
        ConveyerBeltSystems.Instance.AssignBoxNextDesination(this.gameObject);
    }

    public void SetDestination(Transform Destination)
    {
        BoxDestination = Destination;
    }
}
