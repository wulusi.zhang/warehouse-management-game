﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyerBeltSystems : MonoBehaviour
{
    [SerializeField]
    private List<Transform> BeltPositions = new List<Transform>();

    [SerializeField]
    private List<GameObject> BoxesToSpawn = new List<GameObject>();

    [SerializeField]
    private List<GameObject> SpawnedBoxes = new List<GameObject>();

    private static ConveyerBeltSystems instance = null;
    public static ConveyerBeltSystems Instance
    {
        get
        {
            return instance;
        }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
    }
    void Start()
    {
        SpawnBox();
    }

    private void SpawnBox()
    {
        GameObject Currentbox =
            Instantiate(BoxesToSpawn[Random.Range(0, BoxesToSpawn.Count)]);

        //Set box position to the first position after spawning
        Currentbox.transform.position = BeltPositions[0].transform.position;

        //Add it to the list for reference
        SpawnedBoxes.Add(Currentbox);
        MoveBoxes();
    }

    public void OnBoxesMove()
    {
        //Move boxes first then spawn a new one
        SpawnBox();
    }
    private void MoveBoxes()
    {
        for (int i = 0; i < SpawnedBoxes.Count; i++)
        {
            if (SpawnedBoxes[i] == null)
            {
                //Check list for nulls before moving boxes
                SpawnedBoxes.RemoveAt(i);
            }

            SpawnedBoxes[i].GetComponent<BoxMover>().StartMoving();
        }
    }
    public void AssignBoxNextDesination(GameObject Box)
    {
        var BoxIndex = Box.GetComponent<BoxMover>().LocationIndex;

        if (BoxIndex < BeltPositions.Count && (BoxIndex + 1) < BeltPositions.Count)
        {
            Box.GetComponent<BoxMover>().
                SetDestination(BeltPositions[BoxIndex + 1]);
            Box.GetComponent<BoxMover>().LocationIndex++;
        }
        else
        {
            //Box has already reached its finally destination
            //Do nothing or disable the option to move boxes until 
            //It gets processed
        }
    }
}
