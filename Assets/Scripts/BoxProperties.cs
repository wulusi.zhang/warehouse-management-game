﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxProperties : MonoBehaviour
{
    public enum BoxType
    {
        one,
        two,
        three,
        four
    };

    public BoxType m_BoxType;

    public int m_iTargetBinID;

    public int m_iConveyerBeltPositionIndex;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
