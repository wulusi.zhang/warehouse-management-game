﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelPrinter : MonoBehaviour
{
    public Transform label;
    public float startAngle, endAngle, printSpeed; //angles per second
    private float currentAngle;
    private bool isPrinting;

    private WaitForEndOfFrame waitNextFrame;

    private void Start()
    {
        waitNextFrame = new WaitForEndOfFrame();// this way our coroutine will not create garbage
    }

    public void PrintLabel()
    {
        if (!isPrinting)
        {
            StartCoroutine(CR_PrintLabel());
        }
    }

    void RotateLabel(float angle)
    {
        label.localRotation = Quaternion.Euler(new Vector3(angle, 90, -90));
    }

    IEnumerator CR_PrintLabel()
    {
        isPrinting = true;
        currentAngle = startAngle;
        while (currentAngle != endAngle)
        {
            RotateLabel(currentAngle);
            currentAngle = Mathf.Clamp(currentAngle + printSpeed * Time.deltaTime, startAngle, endAngle);
            yield return waitNextFrame;
        }
        yield return null;
    }
}
