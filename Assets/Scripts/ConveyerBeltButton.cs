﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyerBeltButton : MonoBehaviour
{
    [SerializeField]
    private Transform start;

    [SerializeField]
    private Transform end;

    [SerializeField]
    private int ButtonSpeed;

    private bool isButtonPressed;

    // Update is called once per frame
    void Update()
    {
        ButtonAnimation();
    }

    private void ButtonAnimation()
    {
        float step = Time.deltaTime * ButtonSpeed;
        if (isButtonPressed)
        {
            this.transform.position =
                Vector3.MoveTowards(this.transform.position, end.position, step);
        }
        else
        {
            this.transform.position =
                Vector3.MoveTowards(this.transform.position, start.position, step);
        }

        if (Vector3.Distance(this.transform.position, end.position) < 0.5f)
        {
            isButtonPressed = false;
        }
    }

    private void OnMouseUp()
    {
        isButtonPressed = true;
        ConveyerBeltSystems.Instance.OnBoxesMove();
    }
}
