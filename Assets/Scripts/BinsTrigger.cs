﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinsTrigger : MonoBehaviour
{
    [SerializeField]
    private int m_iBinsID;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BoxProperties>())
        {
            if(other.gameObject.GetComponent<BoxProperties>().m_iTargetBinID == m_iBinsID)
            { 
                GameManager.Instance.AddScore();
            
            } else
            {
                GameManager.Instance.SubtractScore();
            }
        }
    }
}
