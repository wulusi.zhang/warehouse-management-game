﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text ScoreText;

    private float m_fTotalScore = 0;

    private static GameManager instance = null;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        ScoreText.text = m_fTotalScore.ToString();
    }

    public void AddScore()
    {
        m_fTotalScore++;
        ScoreText.text = m_fTotalScore.ToString();
    }

    public void SubtractScore()
    {
        m_fTotalScore--;
        ScoreText.text = m_fTotalScore.ToString();
    }
}
